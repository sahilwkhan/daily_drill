const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/



// Q1. Find all the movies with total earnings more than $500M. 
let filmList, movie;
let moviesOver500Million = Object.keys(favouritesMovies).reduce((filmList, movie) => {
    let earnings = parseInt(favouritesMovies[movie].totalEarnings.slice(1));
    if (earnings > 500) {
        filmList[movie] = favouritesMovies[movie];
    };
    return filmList;
}, {});
console.log("Movies with total earnings more than $500M : ");
console.log(moviesOver500Million);
console.log("\n");


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
let movieList, movieForOscarAndEarnings;
let threeOscarsAnd500MillionsFilms = Object.keys(favouritesMovies).reduce((movieList, movieForOscarAndEarnings) => {
    let earningsOfMovie = parseInt(favouritesMovies[movieForOscarAndEarnings].totalEarnings.slice(1));
    if (earningsOfMovie > 500 && favouritesMovies[movieForOscarAndEarnings].oscarNominations > 3) {
        movieList[movieForOscarAndEarnings] = favouritesMovies[movieForOscarAndEarnings];
    };
    return movieList;
}, {});
console.log("Movies with total earnings more than $500M and won more than 3 oscars : ");
console.log(threeOscarsAnd500MillionsFilms);
console.log("\n");

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
let leonardoMovies,movieForActor;
let filmsWithLeonardo = Object.keys(favouritesMovies).reduce( (leonardoMovies,movieForActor) => {
    if (favouritesMovies[movieForActor].actors.includes("Leonardo Dicaprio")) {
        leonardoMovies[movieForActor] = favouritesMovies[movieForActor];
    };
    return leonardoMovies;
}, {});
console.log("Movies with Leonardo as actors : ");
console.log(filmsWithLeonardo);
console.log("\n");

// Q.4 Sort movies (based on IMDB rating)
//     if IMDB ratings are same, compare totalEarning as the secondary metric.
let firstMovie,secondMovie;
let filmsWithSortedRatings = Object.entries(favouritesMovies).sort( (firstMovie,secondMovie) => {
    if (firstMovie[1].imdbRating > secondMovie[1].imdbRating) {
        return -1;
    };
    if (firstMovie[1].imdbRating == secondMovie[1].imdbRating) {
        if (firstMovie[1].totalEarnings > secondMovie[1].totalEarnings) {
            return -1;
        };
    }

});
console.log("Movies sorted according to IMDB rating and then totalEarnings : ");
console.log(filmsWithSortedRatings);
console.log("\n");


// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime

let moviesGenreObject,movieForGenres;
let genresOfFilms = Object.keys(favouritesMovies).reduce( (moviesGenreObject,movieForGenres) => {
    if (favouritesMovies[movieForGenres].genre.includes("drama")) {
        moviesGenreObject.drama.push({ [movieForGenres] :  favouritesMovies[movieForGenres]});
    }
    if (favouritesMovies[movieForGenres].genre.includes("sci-fi")) {
        moviesGenreObject['sci-fi'].push({ [movieForGenres] :  favouritesMovies[movieForGenres]});
    }
    if (favouritesMovies[movieForGenres].genre.includes("adventure")) {
    moviesGenreObject.adventure.push({ [movieForGenres] : favouritesMovies[movieForGenres] });
    }
    if (favouritesMovies[movieForGenres].genre.includes("thriller")) {
        moviesGenreObject.thriller.push({ [movieForGenres]: favouritesMovies[movieForGenres] });
    }
    if (favouritesMovies[movieForGenres].genre.includes("crime")) {
        moviesGenreObject.crime.push({ [movieForGenres] :  favouritesMovies[movieForGenres]});
    }   
    return moviesGenreObject;
}, {drama : [], 'sci-fi' : [],adventure : [],thriller : [], crime : []});
console.log("Movies groups according to genres : ");
console.log(genresOfFilms);
console.log("\n");