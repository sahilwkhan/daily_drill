peopleData = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

// 1. Find all people who are Agender
let agenderPeopleList, person;
let agenderList = peopleData.reduce((agenderPeopleList, person) =>{ 
    if (person.gender == "Agender") {
        agenderPeopleList.push(Object.assign({}, person));
    }
    return agenderPeopleList;
}, []);
// console.log(agenderList);

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
let ipList, personForIp;
let ipComponentsList = peopleData.reduce((ipList, personForIp) => {
    let ipComponents = [];
    let ipPart = "";
    for (let digit in  personForIp.ip_address) {
        if (personForIp.ip_address[digit] == "." ) {
            ipComponents.push(parseInt(ipPart));
            ipPart = "";
        }
        else if(digit == personForIp.ip_address.length-1){
            ipPart += personForIp.ip_address[digit];
            ipComponents.push(parseInt(ipPart));
        } 
        else {
            ipPart += personForIp.ip_address[digit];
        }
    };    
    let newIpObject = Object.assign({}, personForIp);
    newIpObject.ip_address = ipComponents;
    ipList.push( newIpObject );
    return ipList;
}, []);
// console.log(ipComponentsList);


// 3. Find the sum of all the second components of the ip addresses.
let secondPartIpSum, personForSecondIp;
let sumOfAllSecondComponentIp = peopleData.reduce((secondPartIpSum, personForSecondIp) => {
    let ipSecondPart = 0;
    let dotCount = 0;
    for (let SecondDigit of  personForSecondIp.ip_address) {
        if (SecondDigit == ".") {
            dotCount++;
        }
        if (dotCount == 1 && SecondDigit != ".") {
            ipSecondPart +=  parseInt(SecondDigit);
        }
        if (dotCount > 1) {
            break;
        }
    };   
    secondPartIpSum += ipSecondPart;
    return secondPartIpSum;
},0);

// console.log(sumOfAllSecondComponentIp);

// 3. Find the sum of all the fourth components of the ip addresses.
let fourthPartIpSum, personForFourthIp;
let sumOfAllFourthComponentIp = peopleData.reduce((fourthPartIpSum, personForFourthIp) => {
    let ipFourthPart = 0;
    let dotCountForFourth = 0;
    for (let FourthDigit of  personForFourthIp.ip_address) {
        if (FourthDigit == ".") {
            dotCountForFourth++;
        }
        if (dotCountForFourth == 3 && FourthDigit != ".") {
            ipFourthPart +=  parseInt(FourthDigit);
        }
    };   
    fourthPartIpSum += ipFourthPart;
    return fourthPartIpSum;
},0);
// console.log(sumOfAllFourthComponentIp);

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
let fullNameList, personForFullName;
let nameList = peopleData.reduce((fullNameList, personForFullName) =>{ 
    let fullName = personForFullName.first_name + " " + personForFullName.last_name;
    fullNameList.push({Full_name : fullName});
    return fullNameList;
}, []);
// console.log(nameList);

// 5. Filter out all the .org emails
let mailListForOrg, personForOrgMails;
let orgFilteredMailList = peopleData.reduce((mailListForOrg, personForOrgMails) =>{ 
    if (personForOrgMails.email.slice(-4) == ".org") {
        mailListForOrg.push({Email :personForOrgMails.email })
    }
    return mailListForOrg;
}, []);
// console.log(orgFilteredMailList);

// 6. Calculate how many .org, .au, .com emails are there
let domainMailList, personForDomainMails;
domainMailList = { Domain_Counts : { org : 0 , au : 0, com : 0} };
let domainFilteredNameList = peopleData.map((personForDomainMails) => {
    if (personForDomainMails.email.slice(-4) == ".org") {
        domainMailList.Domain_Counts.org++;
    }
    if (personForDomainMails.email.slice(-3) == ".au") {
        domainMailList.Domain_Counts.au++;
    }
    if (personForDomainMails.email.slice(-4) == ".com") {
        domainMailList.Domain_Counts.com++;
    }
});
console.log(domainMailList);

// 7. Sort the data in descending order of first name
let firstName, secondName;
let nameSortedList = Object.assign([],peopleData)
nameSortedList = nameSortedList.sort((firstName, secondName) =>{ 
    if (firstName.first_name > secondName.first_name) {
        return -1;
    }
},);

console.log(nameSortedList);
