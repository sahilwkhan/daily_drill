/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one questiond is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require("fs");

// Store the given data into data.json

fs.writeFile("./data.json",JSON.stringify(data), (errorWritingFile) => {
    if (errorWritingFile) {
        console.error(errorWritingFile);
    }
    else {
        console.log("data.json File Created.\n");
    }
})


//     1. Retrieve data for ids : [2, 13, 23].

fs.readFile("./data.json","utf8", (errorReadingFile,fileDataJSON) => {
    if (errorReadingFile) {
        console.error(errorReadingFile);
    }
    else {
        fileData = JSON.parse(fileDataJSON);
        let idArray = [2, 13, 23];
        let employeeDataForGivenIds = fileData.employees.reduce((filteredIds, employeeObject) => {
            if (idArray.includes(employeeObject.id)) {
                filteredIds.push(employeeObject);
            }
            return filteredIds;
        }, []);

        fs.writeFile("./problem1.json", JSON.stringify(employeeDataForGivenIds), (errorProblem1) => {
            if (errorProblem1) {
                console.error(errorProblem1);
            }
            else {
                console.log("problem1.json File Created.\n");

                solution2(solution3);
            }
        })
    }
})


// 2. Group data based on companies.
// { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function solution2(nextSolution) {
    fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
        if (errorReadingFile) {
            console.error(errorReadingFile);
        }
        else {
            fileData = JSON.parse(fileDataJSON);

            let companiesBasedGroups = fileData.employees.reduce((groupCompanies, employeeDataForCompany) => {
                if (groupCompanies.hasOwnProperty(employeeDataForCompany.company)) {
                    groupCompanies[employeeDataForCompany.company].push({ id: employeeDataForCompany.id, name: employeeDataForCompany.name })
                }
                else {
                    groupCompanies[employeeDataForCompany.company] = [{ id: employeeDataForCompany.id, name: employeeDataForCompany.name }];
                }
                return groupCompanies;
            }, {});
        

            fs.writeFile("./problem2.json", JSON.stringify(companiesBasedGroups), (errorProblem2) => {
                if (errorProblem2) {
                    console.error(errorProblem2);

                }
                else {
                    console.log("problem2.json File Created.\n");
                    nextSolution(solution4);
                }
            })
        }
    })
}


// 3. Get all data for company Powerpuff Brigade
function solution3(nextSolution) {

    fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
        if (errorReadingFile) {
            console.error(errorReadingFile);
        }
        else {
            fileData = JSON.parse(fileDataJSON);

            let dataForPowerpuffBrigade = fileData.employees.reduce((powerpuffData, powerpuffEmployee) => {
                if (powerpuffEmployee.company == "Powerpuff Brigade") {
                    powerpuffData["Powerpuff Brigade"].push({ id: powerpuffEmployee.id, name: powerpuffEmployee.name })
                }
                return powerpuffData;
            }, { "Powerpuff Brigade": [] });

            fs.writeFile("./problem3.json", JSON.stringify(dataForPowerpuffBrigade), (errorProblem3) => {
                if (errorProblem3) {
                    console.error(errorProblem3);

                }
                else {
                    console.log("problem3.json File Created.\n");
                    nextSolution(solution5);
                }
            })
        }
    })
}



// 4. Remove entry with id 2.
    function solution4(nextSolution) {

        fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
            if (errorReadingFile) {
                console.error(errorReadingFile);
            }
            else {
                fileData = JSON.parse(fileDataJSON);

                let employeeDataFiltered = fileData.employees.reduce((removedData, employeeForIdRemoval) => {
                    if (employeeForIdRemoval.id != 2) {
                        removedData.employees.push(employeeForIdRemoval);
                    }
                    return removedData;
                }, { employees: [] });
        
                fs.writeFile("./problem4.json", JSON.stringify(employeeDataFiltered), (errorProblem4) => {
                    if (errorProblem4) {
                        console.error(errorProblem4);
                    }
                    else {
                        console.log("problem4.json File Created.\n");
                        nextSolution(solution6);
                    }
                })

            }
        })
    }

//  5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function solution5(nextSolution) {

    fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
        if (errorReadingFile) {
            console.error(errorReadingFile);
        }
        else {
            fileDataForCopy = JSON.parse(fileDataJSON);

            fileData = JSON.parse(JSON.stringify(fileDataForCopy));
            let sortedData = fileData.employees.sort((firstEmployee, secondEmployee) => {
                if (firstEmployee.company.localeCompare(secondEmployee.company) == -1) {
                    return -1
                }
                if (firstEmployee.company.localeCompare(secondEmployee.company) == 0) {
                    if (firstEmployee.id < secondEmployee.id) {
                        return -1
                    }
                }
            
            });

            fs.writeFile("./problem5.json", JSON.stringify(sortedData), (errorProblem5) => {
                if (errorProblem5) {
                    console.error(errorProblem5);
                }
                else {
                    console.log("problem5.json File Created.\n");
                    nextSolution(solution7);
                }
            })

        }
    })
}

// 6. Swap position of companies with id 93 and id 92.
function solution6(nextSolution) {

    fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
        if (errorReadingFile) {
            console.error(errorReadingFile);
        }
        else {
            fileDataForCopy = JSON.parse(fileDataJSON);

            fileData = JSON.parse(JSON.stringify(fileDataForCopy));
        
            let swapedData = Object.keys(fileData.employees).filter((employeeIndex) => {
                if (fileData.employees[employeeIndex].id == 93 || fileData.employees[employeeIndex].id == 92) {
                    return employeeIndex;
                }
            });

            let tempData = fileData.employees[swapedData[0]].company;
            fileData.employees[swapedData[0]].company = fileData.employees[swapedData[1]].company;
            fileData.employees[swapedData[1]].company = tempData
        
            fs.writeFile("./problem6.json", JSON.stringify(fileData), (errorProblem6) => {
                if (errorProblem6) {
                    console.error(errorProblem6);
                }
                else {
                    console.log("problem6.json File Created.\n");
                    solution7();
                }
            })

        }
    });
}


// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.


function solution7() {

    fs.readFile("./data.json", "utf8", (errorReadingFile, fileDataJSON) => {
        if (errorReadingFile) {
            console.error(errorReadingFile);
        }
        else {
            fileData = JSON.parse(fileDataJSON);

            let employeeDataForBirthday = fileData.employees.reduce((removedData, employeeForBirthday) => {
                if (employeeForBirthday.id % 2 == 0) {
                    let birthday = Object.assign({}, employeeForBirthday)
                    let current = new Date();
                    let year = Math.floor(Math.random() * (30 - 18)) + 18;
                    let brithDate = `${current.getDate()}/${current.getMonth()}/${current.getFullYear() - year}`
                    employeeForBirthday.brithday = brithDate;
                    removedData.employees.push(employeeForBirthday);
                }
                return removedData;
            }, { employees: [] });
    
            fs.writeFile("./problem7.json", JSON.stringify(employeeDataForBirthday), (errorProblem7) => {
                if (errorProblem7) {
                    console.error(errorProblem7);
                }
                else {
                    console.log("problem7.json File Created.\n");
                }
            })

        }
    })
}
