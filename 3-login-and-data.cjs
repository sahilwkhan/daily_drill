/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/







function login(user, val) {
    if(val % 2 === 1) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    let time = Date()
    createFilePromise(path.join(__dirname, user + " activity log", `Time = ${time.getHours}:${time.getMinutes()}, Activity = ${activity}`));

}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


const fs = require('fs');
const util = require('util');
const path = require('path');

const createFilePromise = util.promisify(fs.writeFile);
const deleteFilePromise = util.promisify(fs.unlink);
const readFilePromise = util.promisify(fs.readFile);


// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


// First create 2 files 
function createAndDeleteFiles() {

    function create2FileSimultaneously() {
        return new Promise((resolve, reject) => {
            createFilePromise(path.join(__dirname, "/File_1.json"), "This is first file using promisified fs writefile function.");
            createFilePromise(path.join(__dirname, "/File_2.json"), "This is second file using promisified fs writefile function.");
            return resolve("File_1.json and File_2.json created.")
        })
    }

    create2FileSimultaneously()
        .then((createMessage) => {
            console.log(createMessage);
        })
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve("Code delayed by 2 seconds");
                }, 2000);
            })
        })
        .then((delayMessage) => {
            console.log(delayMessage);
            return deleteFilePromise(path.join(__dirname, "/File_1.json"));
        })
        .then(() => {
            console.log("File 1 deleted.");
            return deleteFilePromise(path.join(__dirname, "/File_2.json"));
        })
        .then(() => {
            console.log("File 2 deleted.");
        })
        .catch((error) => {
            console.log("createAndDeleteFiles function failed to execute :-");
            console.error(error);
        })
}
createAndDeleteFiles();



// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining


function createReadDeleteLipsumData() {
    
    const lipsumData = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

    createFilePromise(path.join(__dirname, "/lipsum.txt"), lipsumData)
        .then(() => {
            console.log("lipsum.txt created.");
            return readFilePromise(path.join(__dirname, "/lipsum.txt"), "utf-8");
        })
        .then((fileData) => {
            console.log("Lipsum Data :-\n" + fileData);
            return createFilePromise(path.join(__dirname, "/secondLipsum.txt"), fileData)
        })
        .then(() => {
            console.log("Second lipsum data file created.");
            return deleteFilePromise(path.join(__dirname, "/lipsum.txt"));
        })
        .then(()=> {
            console.log("lipsum.txt file deleted.");
        })
        .catch((error) => {
            console.log("createReadDeleteLipsumData function failed to execute :-");
            console.error(error);
        })
}
createReadDeleteLipsumData();



/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function loginWith3Value() {

    login("User_1", 3)
        .then((loginOutput) => {
            console.log(loginOutput);
            return getData();
        })
        .then((dataOutput) => {
            console.log(dataOutput);
        })
        .catch((loginError) => {
            console.log("Unable to login with value 3:- " + loginError);
            console.log("Logging with value 2 :-");
            // login("User_1", 2)
            // .then((loginOutput) => {
            //     console.log(loginOutput);
            //     return getData();
            // })
            // .then((dataOutput) => {
            //     console.log(dataOutput);
            // })
        })
}

loginWith3Value();
// d = new Date();
// console.log(d.getHours(),d.getMinutes() );