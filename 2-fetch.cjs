
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const path = require("path");
const fs = require("fs");


fetchAllUsers("https://jsonplaceholder.typicode.com/users")
    .then((userData) => {   
        fs.writeFile(path.join(__dirname, "Solution_1.json"), JSON.stringify(userData), (errorWritingFile) => {
            if (errorWritingFile) {
                console.log("Error Writing File for Solution_1.json : ");
                console.log(errorWritingFile);
            } 
            else {
                console.log("Solution_1.json written successful.");
            }
        });
    })
    .then(() => {
        return fetchAllTodos("https://jsonplaceholder.typicode.com/todos");
    })
    .then((todoData) => {
        fs.writeFile(path.join(__dirname, "Solution_2.json"), JSON.stringify(todoData), (errorWritingFile) => {
            if (errorWritingFile) {
                console.log("Error Writing File for Solution_1.json : ");
                console.log(errorWritingFile);
            } 
            else {
                console.log("Solution_2.json written successful.");
            }
        });
    })
    .then(() => {
        fetchAllUsers("https://jsonplaceholder.typicode.com/users")
            .then((userData3) => {
                fs.writeFile(path.join(__dirname, "Solution_3.json"), JSON.stringify(userData3), (errorWritingFile) => {
                    if (errorWritingFile) {
                        console.log("Error Writing File for Solution_1.json : ");
                        console.log(errorWritingFile);
                    }
                    else {
                        console.log("Solution_3.json written successful.");
                    }
                });
            }).then(() => {
                return fetchAllTodos("https://jsonplaceholder.typicode.com/todos")
            })
            .then((todoData3) => {
                fs.appendFile(path.join(__dirname, "Solution_3.json"), JSON.stringify(todoData3), (errorWritingFile) => {
                    if (errorWritingFile) {
                        console.log("Error Writing File for Solution_1.json : ");
                        console.log(errorWritingFile);
                    }
                    else {
                        console.log("Solution_3.json written successful.");
                    }
                });
            })
        
    })
    .catch((error) => {
        console.error(error);
    })
    


    
// 1. Fetch all the users

function fetchAllUsers(userLink) {

    return new Promise((resolve, reject) => {
        fetch(userLink)
            .then((usersData) => {
                if (usersData.ok) {
                    console.log("Request for users data was successful.");
                    return usersData.json();
                }
                else {
                    return new Error("Users response failed.")
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch((errorFetchingRequest) => {
                console.log("Error recieved while loading users data : \n");
                reject(errorFetchingRequest);
            });
    });

}

// 2. Fetch all the todos

function fetchAllTodos(todoLink) {

    return new Promise((resolve, reject) => {
        fetch(todoLink)
            .then((todosData) => {
                if (todosData.ok) {
                    console.log("Request for todos data was successful.");
                    return todosData.json();
                }
                else {
                    return new Error("Todos Data request failed.")
                }
            })
            .then((todoLoadedData) => {
                resolve(todoLoadedData);
            })
            .catch((errorFetchingRequest) => {
                console.log("Error recieved while loading todos data : \n");
                reject(errorFetchingRequest);
            });
        });
}


