const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

// 1. Get all items that are available 

let avaiableItemsList, singleItem;
let itemsAvailable = items.reduce((avaiableItemsList,singleItem) =>{
    if(singleItem.available){
        avaiableItemsList.Available_Items.push(singleItem.name);
    }
    return avaiableItemsList;
},{Available_Items : []})

console.log(itemsAvailable);

// 2.Get all items containing only Vitamin C.
let vitaminsCList, fruit;
let fruitsWithVitaminC = items.reduce((vitaminsCList,fruit) =>{
    if(fruit.contains.indexOf("Vitamin C") > -1){
        vitaminsCList.Fruits_with_Vitamin_C.push(fruit.name);
    }
    return vitaminsCList;
},{Fruits_with_Vitamin_C : []})
console.log(fruitsWithVitaminC);

// 3. Get all items containing Vitamin A.
let vitaminsAList, fruitItem;
let fruitsWithVitaminA = items.reduce((vitaminsAList,fruitItem) =>{
    if(fruitItem.contains.indexOf("Vitamin A") > -1){
        vitaminsAList.Fruits_with_Vitamin_A.push(fruitItem.name);
    }
    return vitaminsAList;
},{Fruits_with_Vitamin_A : []})
console.log(fruitsWithVitaminA);

// 4. Group items based on the Vitamins that they contain.
let vitaminFruitList, fruitForVitamin;
let vitaminsObject = items.reduce((vitaminFruitList,fruitForVitamin) =>{
    let vitaminArray, singleVitamin;

    if (fruitForVitamin.contains.indexOf(',') > -1){
        let vitaminArray = fruitForVitamin.contains.split(", ");
        
        vitaminArray.map((singleVitamin)=>{
            if(vitaminFruitList.hasOwnProperty(singleVitamin)){
                vitaminFruitList[singleVitamin].push(fruitForVitamin.name);
            }
            else{
                vitaminFruitList[singleVitamin] = [fruitForVitamin.name];
            }
        });
    }
    else{
        if(vitaminFruitList.hasOwnProperty(fruitForVitamin.contains)){
            vitaminFruitList[fruitForVitamin.contains].push(fruitForVitamin.name);
        }
        else{
            vitaminFruitList[fruitForVitamin.contains] = [fruitForVitamin.name];
        }
    }

    return vitaminFruitList;
},{ })
console.log(vitaminsObject);

// 5. Sort items based on number of Vitamins they contain.
let ObjectForCountingVitamin, vitaminCountFruit,firstFruit,secondFruit;
let vitaminNumberCount = items.reduce((ObjectForCountingVitamin,vitaminCountFruit) =>{
    
    if (vitaminCountFruit.contains.indexOf(',') > -1){
        let vitaminArray = vitaminCountFruit.contains.split(", ");
        ObjectForCountingVitamin.push( { Fruit :  vitaminCountFruit.name, Number_of_Vitamins :  vitaminArray.length});

    }
    else{
        ObjectForCountingVitamin.push( { Fruit :  vitaminCountFruit.name, Number_of_Vitamins :  1});
    }

    return ObjectForCountingVitamin;
},[]).sort( (firstFruit, secondFruit)=>{
    if( firstFruit.Number_of_Vitamins > secondFruit.Number_of_Vitamins ){
    return -1
    }
})
console.log(vitaminNumberCount);
