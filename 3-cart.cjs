const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/



// Q1. Find all the items with price more than $65.
let itemsCostlierThan65Dollars = Object.keys(products[0]).reduce((pricedFiltering,item) => {
    
    if (products[0][item].hasOwnProperty('price')) {
        if (parseInt(products[0][item].price.slice(1)) > 65) {
            pricedFiltering.push({ [item] : products[0][item] });
        } 
    }
    if (Array.isArray(products[0][item])) {

        let filteredProducts2 = products[0][item].filter((item2) => {
            if (parseInt(Object.values(item2)[0].price.slice(1)) > 65) {
                return item2;
            }
        });
        let nestedArray = filteredProducts2.reduce((objectsArray, filterItem) => {
            objectsArray.push(filterItem);
            return objectsArray;
        }, []);
        pricedFiltering = pricedFiltering.concat(nestedArray);
    }
    return pricedFiltering;
}, [] );
console.log("All the items with price more than $65 :- ");
console.log(itemsCostlierThan65Dollars);
console.log("\n");

// Q2. Find all the items where quantity ordered is more than 1.
let itemsHavingQuantityMoreThan1 = Object.keys(products[0]).reduce((quantityFiltering,itemForQuantity) => {
    
    if (products[0][itemForQuantity].hasOwnProperty('quantity')) {
        if (products[0][itemForQuantity].quantity > 1) {
            quantityFiltering.push({ [itemForQuantity] : products[0][itemForQuantity] });
        } 
    }
    if (Array.isArray(products[0][itemForQuantity])) {

        let filteredProductsForQuantity = products[0][itemForQuantity].filter((item2) => {
            if (Object.values(item2)[0].quantity > 1) {
                return item2;
            }
        });
        
        let nestedArray = filteredProductsForQuantity.reduce((objectsArray, filterItemForQuantity) => {
            objectsArray.push(filterItemForQuantity);
            return objectsArray;
        }, []);
        quantityFiltering = quantityFiltering.concat(nestedArray);
    }
    return quantityFiltering;
}, [] );
console.log(" All the items where quantity ordered is more than 1 :- ");
console.log(itemsHavingQuantityMoreThan1);
console.log("\n");

// Q.3 Get all items which are mentioned as fragile.
let fragileItems = Object.keys(products[0]).reduce((fragileFiltering,itemForType) => {
    
    if (products[0][itemForType].hasOwnProperty('type')) {
        if (products[0][itemForType].type == "fragile") {
            fragileFiltering.push({ [itemForType] : products[0][itemForType] });
        } 
    }
    if (Array.isArray(products[0][itemForType])) {

        let filteredProductsForFragileItems = products[0][itemForType].filter((item2) => {
            if (Object.values(item2)[0].type == 'fragile') {
                return item2;
            }
        });
        
        let nestedArray = filteredProductsForFragileItems.reduce((objectsArray, filterFragileItems) => {
            objectsArray.push(filterFragileItems);
            return objectsArray;
        }, []);
        fragileFiltering = fragileFiltering.concat(nestedArray);
    }
    return fragileFiltering;
}, [] );
console.log(" All items which are mentioned as fragile :- ");
console.log(fragileItems);
console.log("\n");

// Q.4 Find the least and the most expensive item for a single quantity.
let leastAndMostExpensiveItems = Object.keys(products[0]).reduce((costFiltering, itemForCost) => {
    
    if (products[0][itemForCost].hasOwnProperty('quantity')) {
        if (products[0][itemForCost].quantity == 1) {
            costFiltering.push({ [itemForCost]: products[0][itemForCost] });
        }
    }
    if (Array.isArray(products[0][itemForCost])) {

        let filteredProductsForCost = products[0][itemForCost].filter((item2) => {
            if (Object.values(item2)[0].quantity == 1) {
                return item2;
            }
        });
        
        let nestedArray = filteredProductsForCost.reduce((objectsArray, filterQuantityItems) => {
            objectsArray.push(filterQuantityItems);
            return objectsArray;
        }, []);
        costFiltering = costFiltering.concat(nestedArray);
    }
    return costFiltering;
}, [])
    .sort((firstItems, secondItems) => {
    if (Object.values(firstItems)[0].price > Object.values(firstItems)[0].price) {
        return -1
        }
    })
    .reduce((finalList, item3, currentIndex, ObjectArray) => {
        if (currentIndex == 0) {
            finalList.push({ "Least Priced Item" : item3})
        }
        if (currentIndex == ObjectArray.length-1) {
            finalList.push({ "Highest Priced Item" : item3})
        }
        return finalList;
},[])

console.log(" The least and the most expensive item for a single quantity :- ");
console.log(leastAndMostExpensiveItems);
console.log("\n");

// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
let itemsAsPerTheirState = Object.keys(products[0]).reduce((stateFiltering, itemForState) => {
    
    if (itemForState.indexOf("oil") > -1 || itemForState.indexOf("Oil") > -1 ) {
        stateFiltering.Liquid.push({ [itemForState] : products[0][itemForState]});
    }
    if (itemForState.indexOf("shampoo") > -1  || itemForState.indexOf("Shampoo") > -1) {
        stateFiltering.Liquid.push({ [itemForState]: products[0][itemForState]});
    }
    // else if (Array.isArray(products[0][itemForState])) {

    //     let filteredProductsForStateLiquid = products[0][itemForState].filter((item2) => {
    //         if (Object.keys(item2) == "shampoo" || Object.keys(item2) == "oil") {
    //             return item2;
    //         }
    //     });
    //     if (filteredProductsForStateLiquid.length != 0) {
    //         stateFiltering.Liquid.push({ [itemForState]: item2});
    //     }
        
    // }
    if (itemForState.indexOf("oil") == -1 && itemForState.indexOf("Oil") == -1) {
        if (itemForState.indexOf("shampoo") == -1 && itemForState.indexOf("Shampoo") == -1) {
            stateFiltering.Solid.push({ [itemForState]: products[0][itemForState] });
        }
    }
    return stateFiltering;
}, {"Liquid" : [], Solid : []});

console.log("  Group items based on their state of matter at room temperature (Solid, Liquid Gas) :- ");
console.log(itemsAsPerTheirState);
console.log("\n");

