const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/


// Q1 Find all users who are interested in playing video games.
let peopleInterestedInGames = Object.keys(users).reduce((nameList, person) => {

    if (users[person].interest) {
        if (users[person].interest.toString().indexOf("Video Games") > -1) {
            nameList.push({ name: person, Interest: "Playing Video Games" });
        }
    }
    else{
        if ( users[person].interests.toString().indexOf("Video Games") > -1) {
            nameList.push({ name: person, Interest: "Playing Video Games" });
        }
    };

    return nameList;
}, []);
console.log(peopleInterestedInGames);
console.log("\n");

// Q2 Find all users staying in Germany.
let germanyUsers = Object.keys(users).reduce((countryUsers, personForNationality) => {

    if (users[personForNationality].nationality) {
        if (users[personForNationality].nationality === "Germany") {
            if (countryUsers["Users from Germany"]) {
                countryUsers["Users from Germany"].push(personForNationality);
            }
            else {
                countryUsers["Users from Germany"] = [personForNationality];
            }
        }
    }

    return countryUsers;
}, {});

console.log(germanyUsers);
console.log("\n");

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

let seniorityWiseUsers = Object.entries(users).sort((firstUser, secondUser) => {
    if (firstUser[1].desgination.indexOf("Senior") > -1) {
        if (secondUser[1].desgination.indexOf("Senior") > -1) {
            if (firstUser[1].age > secondUser[1].age) {
                return -1;
            }
        }
        if (secondUser[1].desgination.indexOf("Developer") > -1 && secondUser[1].desgination.indexOf("Senior") == -1) {
            return -1;
        }
        if (secondUser[1].desgination.indexOf("Intern") > -1) {
            return -1;
        }
    }   
    if (firstUser[1].desgination.indexOf("Developer") > -1) {
        if (secondUser[1].desgination.indexOf("Developer") > -1 && secondUser[1].desgination.indexOf("Senior") == -1) {
            if (firstUser[1].age > secondUser[1].age) {
                return -1;
            }
        }
        if (secondUser[1].desgination.indexOf("Intern") > -1 ) {
            return -1
        }
    } 
    else {
        if (secondUser[1].desgination.indexOf("Intern") > -1 ) {
            return -1
        }
        return 0;
    } 
});
console.log(seniorityWiseUsers);
console.log("\n");


// Q4 Find all users with masters Degree.
let usersWithMasterDegree = Object.keys(users).filter((userForDegree) => {
    if (users[userForDegree].qualification == "Masters") {
        return users;
   } 
});

console.log({ "Users with masters Degree" : usersWithMasterDegree});
console.log("\n");

// Q5 Group users based on their Programming language mentioned in their designation.
let programmingLanguagesAvailable = ["Python", "Javascript", "Golang"]

let languageWiseUsers = Object.keys(users).reduce((languageObject, personForProgrammingLanguage) => {
    designationWords = users[personForProgrammingLanguage].desgination.split(" ");
    
    let languageOfPerson = designationWords.filter((language) => {
        if (programmingLanguagesAvailable.includes(language)) {
            return language;
        };
    })
    if (languageObject[languageOfPerson]) {
        languageObject[languageOfPerson].push(personForProgrammingLanguage);
    }
    else {
        languageObject[languageOfPerson] = [personForProgrammingLanguage];
    }
    return languageObject;
}, {});

console.log(languageWiseUsers);
console.log("\n");